export let authentication = (to, from, next) => {
  if (to.meta.authentication && localStorage.getItem("userCredentials")) {
    next();
  } else if (
    to.meta.authentication &&
    !localStorage.getItem("userCredentials")
  ) {
    next({ name: "Login" });
  } else {
    next();
  }
};
