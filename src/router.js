import Vue from "vue";
import Router from "vue-router";
import Register from "./views/Register.vue";
import Home from "./views/Home";
import NewsHeadlines from "./components/home/NewsHeadlines";
import { authentication } from "./__helper__/AuthenticationHandler";

Vue.use(Router);
let router = new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "Login",
      component: Register,
      meta: {
        authentication: false
      }
    },
    {
      path: "/home",
      component: Home,
      children: [
        {
          path: "/",
          name: "news",
          component: NewsHeadlines,
          meta: {
            authentication: true
          }
        }
      ],
      meta: {
        authentication: true
      }
    }
  ]
});

router.beforeEach(authentication);

export default router;
